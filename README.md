# Pycalculix DXF import #

Python script that adds dxf import functionality to Pycalculix (tool for build finite elment analysis in Python)

Uses [dxfgrabber](https://bitbucket.org/mozman/dxfgrabber) for dxf reading.


![example1_dam_S3-300x267.png](https://bitbucket.org/repo/9jaAqR/images/581545851-example1_dam_S3-300x267.png)

For [Pycalculix](http://justinablack.com/pycalculix/) all credits goes to Justin Black