###########################################################################
#
# program: entities.py
# author: Ales Vozelj
# Email: ales.vozelj@gmail.com
# version: 1.4
# date: march 2, 2015
# description:  Arc and Line objects definition
#
###########################################################################
from math import cos,sin,radians, ceil
from copy import copy

def precision(x):
        return float("{:.4f}".format(x))

############################################
# rotation of coordinate system
############################################

def transform(x,y,fi):
    fi=radians(fi)
    xt=x*cos(fi)+y*sin(fi)
    yt=-x*sin(fi)+y*cos(fi)
    return xt,yt

############################################
# Arcs
############################################

class Arc:
    def __init__(self,arc):
        self.r=arc.radius
        self.startangle=arc.startangle
        self.endangle=arc.endangle
        self.xcenter=arc.center[0]
        self.ycenter=arc.center[1]
        self.xstart=self.getx(self.xcenter, self.r, self.startangle)
        self.ystart=self.gety(self.ycenter, self.r, self.startangle)
        self.xend=self.getx(self.xcenter, self.r, self.endangle)
        self.yend=self.gety(self.ycenter, self.r, self.endangle)
        self.deltaangle=self.endangle-self.startangle
        if self.endangle<self.startangle:
            self.deltaangle=360.0+self.deltaangle

    def getx(self,x,r,fi):
        fi=radians(fi)
        return precision(x+r*cos(fi))

    def gety(self,y,r,fi):
        fi=radians(fi)
        return precision(y+r*sin(fi))

    def switchstartend(self):
        xe=self.xstart
        ye=self.ystart
        xs=self.xend
        ys=self.yend
        endangle=self.startangle
        startangle=self.endangle
        self.xstart=xs
        self.ystart=ys
        self.xend=xe
        self.yend=ye
        self.startangle=startangle
        self.endangle=endangle

    def createarcsegments(self):
        segments=[]
        if self.deltaangle>90.0:
            ns=int(ceil(self.deltaangle/90.0)) #numberofsegments
            segmentangle=float(self.deltaangle/ns)
            # let's rotate CS to startposition
            sa=0
            for i in range(0,ns):
                kot=(i+1)*segmentangle
                startangle=self.to360deg(sa+self.startangle)
                endangle=self.to360deg(self.startangle+kot)
                sa=kot
                # create segment
                arcsegment=copy(self)
                arcsegment.transformentity(startangle,endangle)
                segments.append(arcsegment)
        else:
            segments.append(self)
        return segments

    def to360deg(self,angle):
        if angle>=360.0:
            angle-=360.0
        return angle

    def rotatecordinates(self,fi):
        xs=self.xstart
        ys=self.ystart
        xe=self.xend
        ye=self.yend
        xc=self.xcenter
        yc=self.ycenter
        self.xstart,self.ystart=transform(xs,ys,fi)
        self.xend,self.yend=transform(xe,ye,fi)
        self.xcenter,self.ycenter=transform(xc,yc,fi)
        self.startangle=self.startangle-fi
        self.endangle=self.endangle-fi
        return self

    def transformentity(self,startangle,endangle):
        self.xstart=self.getx(self.xcenter, self.r, startangle)
        self.ystart=self.gety(self.ycenter, self.r, startangle)
        self.xend=self.getx(self.xcenter, self.r, endangle)
        self.yend=self.gety(self.ycenter, self.r, endangle)
        self.startangle=startangle
        self.endangle=endangle
        self.deltaangle=self.endangle-self.startangle
        if self.endangle<self.startangle:
            self.deltaangle=360.0+self.deltaangle
        return self

############################################
# Lines
############################################

class Line:
    def __init__(self,line):
        self.xstart=precision(line.start[0])
        self.ystart=precision(line.start[1])
        self.xend=precision(line.end[0])
        self.yend=precision(line.end[1])

    def switchstartend(self):
        xe=self.xstart
        ye=self.ystart
        xs=self.xend
        ys=self.yend
        self.xstart=xs
        self.ystart=ys
        self.xend=xe
        self.yend=ye

    def rotatecordinates(self,fi):
        xs=self.xstart
        ys=self.ystart
        xe=self.xend
        ye=self.yend
        self.xstart,self.ystart=transform(xs,ys,fi)
        self.xend,self.yend=transform(xe,ye,fi)
        return self