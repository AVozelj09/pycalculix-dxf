'''
Created on 4. mar. 2015

@author: ales
'''
from dxfimport import DxfImport
from entities import Arc,Line
import pycalculix as pyc
import dxfgrabber

dx=DxfImport('test3.dxf',transformationangle=90.0)
# transformation since we usnually draw y as radial and x as axial
curves=dx.getloop()
title="kontrola"
model = pyc.FeaModel(title)
model.set_units('mm')
# make part
part = pyc.Part(model)
print('goto',curves[0].xstart,curves[0].ystart)
part.goto(curves[0].xstart,curves[0].ystart)
lines = []
for curve in curves:
    if isinstance(curve,Arc):
        part.draw_arc(curve.xend,curve.yend,curve.xcenter,curve.ycenter)
    if isinstance(curve,Line):
        [L, p1, p2] = part.draw_line_to(curve.xend,curve.yend)
        lines.append(L)

model.plot_geometry(title+'_geom')