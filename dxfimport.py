###########################################################################
#
# program: dxfimport.py
# author: Ales Vozelj
# Email: ales.vozelj@gmail.com
# version: 1.4
# date: march 2, 2015
# description:  import 2d dxf loop and creates gmesh loop entities
#
###########################################################################

import numpy as np
import dxfgrabber
from entities import Line,Arc
from copy import copy
from math import radians,cos,sin
#from drawdxf import Draw

itemslist=[]

class DxfImport:
    def __init__(self,filename,layer='0',transformationangle=0.0):
        self.filename=filename
        self.layer='0'
        self.transformationangle=transformationangle

##############################
# main call
##############################

    def getloop(self):
        # main call of this class
        # returns list of Arc and Line entities ordered in a clockwise loop
        # Arc entities are segments of less than 90�
        self.readdxffile()
        self.getuniquecoordinates()
        loop=self.setloopclockwise(self.getlooppnts())
        finalloop=self.getentities(loop)
        return finalloop


##############################
# properties
##############################

    def getlines(self):
        global dxflines
        self.setlines(dxflines)
        return dxflines

    def setlines(self,val):
        global dxflines
        dxflines= val

    def getarcs(self):
        global dxfarcs
        self.setarcs(dxfarcs)
        return dxfarcs

    def setarcs(self,val):
        global dxfarcs
        dxfarcs= val

    def getpnts(self):
        global dxfpnts
        self.setpnts(dxfpnts)
        return dxfpnts

    def setpnts(self,val):
        global dxfpnts
        dxfpnts= val
##############################
# subroutines
##############################

    def getentities(self,loop):
        # takes ordered loop
        # and creates final ordered list of entities
        # also do a coordinate transformation on entities
        finallist=[]
        curveslist=[]
        for row in loop:
            entity=itemslist[row[2]] #third column is index column
            pntorder=self.getentitypntid(entity,addindex=False)
            if row[0]!=pntorder[0]: #if end and start of curve is switched
                entity.switchstartend()
            curveslist.append(entity)

        finallist=[] #clean
        for curve in curveslist:
            finallist.append(curve.rotatecordinates(self.transformationangle))
        return finallist

##############################
# unique coordinates
##############################

    def getuniquecoordinates(self):
        # gets all start,end cordinates
        # of curves and filter just
        # unique ones
        # returns as [X,Y] array

        PNT=[]
        for line in self.getlines():
            PNT.append(line.xstart)
            PNT.append(line.ystart)
            PNT.append(line.xend)
            PNT.append(line.yend)
        for arc in self.getarcs():
            PNT.append(arc.xstart)
            PNT.append(arc.ystart)
            PNT.append(arc.xend)
            PNT.append(arc.yend)
        pt=np.resize(np.asarray(PNT),(len(PNT)/2,2))
        # save list of unique coordinates
        self.setpnts(self.unique_rows(pt))
        self.getpnts()

##############################
# indexed array of points
##############################

    def getlooppnts(self):
        # instead of coordinates use indexing of points
        # and create ordered loop of them
        # returns array [startpointid,endpointid,entityid]
        looppts=[]
        loop_=[]
        for line in self.getlines():
            loop_=looppts+self.getentitypntid(line)
            looppts=loop_
        for arc in self.getarcs():
            loop_=looppts+self.getentitypntid(arc)
            looppts=loop_
        return self.OrderPNTloop(np.resize(np.asarray(looppts),(len(looppts)/3,3)))

############################################
# finds point index based on coordinate
############################################

    def getentitypntid(self,entity,addindex=True):
        # converts x,y coordinates of entity to points indexes
        # returns list with [startpointindex,endpointindex,entityindex]
        # last entityindex return is optional
        entitypnts=[]
        pt=self.getpnts()
        # startpnt
        s_for=[]
        s_for.append(entity.xstart)
        s_for.append(entity.ystart)
        s=np.resize(np.asarray(s_for),2)
        out=np.where(np.all(pt==s,axis=1))
        entitypnts.append(out[0][0])
        # endpnt
        s_for=[]
        s_for.append(entity.xend)
        s_for.append(entity.yend)
        s=np.resize(np.asarray(s_for),2)
        out=np.where(np.all(pt==s,axis=1))
        entitypnts.append(out[0][0])
        if addindex:
            itemslist.append(entity)
            entitypnts.append(self.last_index(itemslist)) # indexing entity
        return entitypnts

############################################
# using dxfgrabber reads dxf
############################################

    def readdxffile(self):
        #reads entities of dxf and saves them to properties
        dwg = dxfgrabber.readfile(self.filename)
        all_lines = [entity for entity in dwg.entities if (entity.dxftype == 'LINE' and entity.layer == self.layer)]
        all_arcs = [entity for entity in dwg.entities if (entity.dxftype == 'ARC' and entity.layer == self.layer)]
        lines=[]
        arcs=[]
        for l in all_lines:
            line=Line(l)
            lines.append(line)
        for a in all_arcs:
            arc=Arc(a)
            if arc.deltaangle>90.0:
                arcs_=arcs+arc.createarcsegments()
                arcs=arcs_
            else:
                arcs.append(arc)
        self.setlines(lines)
        self.setarcs(arcs)
        print(arcs)

############################################
# find's loop next point id
############################################

    def GetNextPTid(self,pt,arr,out):
        # get next point id based on current and entity
        # which is appended to current
        rid=-1
        ort=-1
        fid=None
        b_lis=[]
        if arr is not None:
            if arr.ndim==1:
                rid=0
            else:
                x=np.where(arr[:,0:2]==pt)
                if x[0].shape[0]>0:
                    rid=x[0][0]
                    ort=self.switch(x[1][0])
            if rid>-1:
                b=arr[rid]
                if ort==0:
                    b_lis.append(b[1])
                    b_lis.append(b[0])
                    b_lis.append(b[2])
                    bcol=np.asarray(b_lis)
                else:
                    bcol=b
                c=np.vstack((out,bcol))
                out=c
                if ort>-1:
                    fid=b[ort]
                    c=np.delete(arr,[rid],axis=0)
                    arr=c
                else:
                    arr=None
        return fid,arr,out

############################################
# order list of points into a loop
############################################

    def OrderPNTloop(self,pt):
        #order array of points into ordered loop
        loop=pt[pt[:,0].argsort()] #sort by start point
        a=loop[0] #first entity
        fid=a[1] #end point of first entity
        b=np.delete(loop,[0],axis=0) #remove first entity from loop
        loop=b
        del b
        #find and order points to follow loop
        for i in range(0,loop.shape[0]):
            fid,loop,a=self.GetNextPTid(fid,loop,a)
        return a

############################################
# makes loop orientation clockwise
############################################

    def setloopclockwise(self,loop):
        #set loop to clockwise direction
        lsum=0
        coordinates=self.getpnts()
        for pts in loop:
            lsum+=(coordinates[pts[1],0]-coordinates[pts[0],0])*(coordinates[pts[1],1]+coordinates[pts[0],1])
        if lsum>0: # because we have radial-x dir (^ up) and axial-y dir (> right)
            loop_rev=loop[::-1]
            #flip first and second column
            i = np.array([1, 0, 2])
            loop=loop_rev[:,i]
        return loop

############################################
# returns only unique rows from an array
############################################

    def unique_rows(self,a):
        # returns unique array of x,y coordinates
        a = np.ascontiguousarray(a)
        unique_a = np.unique(a.view([('', a.dtype)]*a.shape[1]))
        return unique_a.view(a.dtype).reshape((unique_a.shape[0], a.shape[1]))

############################################
# last list index
############################################

    def last_index(self,a):
        return len(a)-1

############################################
# switch
############################################

    def switch(self,x):
        if x==0:
            return 1
        else:
            return 0

###############################################
# area of a loop - testing purpose only below
###############################################

    def polygonarea(self,curves):
        X=[]
        Y=[]
        X.append(curves[0].xstart)
        Y.append(curves[0].ystart)
        for curve in curves:
            if isinstance(curve,Arc):
                x,y=self.convertarctopolygon(curve.r,curve.startangle,curve.endangle,curve.xcenter,curve.ycenter)
                X_=X+x
                X=X_
                Y_=Y+y
                Y=Y_
            if isinstance(curve,Line):
                X.append(curve.xend)
                Y.append(curve.yend)
        area=0
        numPoints=len(X)
        j=numPoints-1
        for i in range(0,numPoints):
            area+=(X[j]+X[i])*(Y[j]-Y[i])
            j=i
        #
        coord=np.vstack((np.asarray(X),np.asarray(Y)))
        #Draw('polygon.dxf',coord.T)
        #
        return area/2

    def convertarctopolygon(self,r,anglestart,angleend,xc,yc,N=6):
        x=[]
        y=[]
        delangle=(angleend-anglestart)/N
        for i in range(1,N+1):
            x.append(self.getx(xc,r,anglestart+delangle*i))
            y.append(self.gety(yc,r,anglestart+delangle*i))
        return x,y

    def getx(self,x,r,fi):
        fi=radians(fi)
        return x+r*cos(fi)

    def gety(self,y,r,fi):
        fi=radians(fi)
        return y+r*sin(fi)
